/**
 * @file
 *
 */

(function ($) {

  Drupal.behaviors.entity_connections = {
    attach: function (context, settings) {
      var graph_div = $('#entity_connections');
      graph_div.attr('width', graph_div.parent().width());

      var stiffness = Drupal.settings.entity_connections.settings.stiffness;
      var repulsion = Drupal.settings.entity_connections.settings.repulsion;
      var friction = Drupal.settings.entity_connections.settings.friction;

      var sys = arbor.ParticleSystem();
      sys.parameters({
        stiffness: stiffness,// Merevség
        repulsion: repulsion,// Node taszítás
        friction: friction,// Node taszítás
        gravity: true,
        dt: 0.015
      });
      sys.renderer = Renderer("#entity_connections");

      var data = {
        nodes: Drupal.settings.entity_connections.nodes,
        edges: Drupal.settings.entity_connections.edges
      };
      sys.graft(data);

    }
  }
})(this.jQuery)
