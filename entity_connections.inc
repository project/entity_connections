<?php
define('EC_DEFAULT_COLOR', '#000000');

class EntityConnections {

  public $nodes = array();
  public $connections;
  private $available_field_type = array(
    'node_reference',
    'entityreference',
    'taxonomy_term_reference',
    'commerce_product_reference',
    'commerce_line_item_reference',
  );
  public $known_entities = array();
  private $field_collection_bundles = array();
  private $entity_colors = array(
    'node' => '#008000',
    'taxonomy_term' => '#800000',
    'user' => '#000099',
    'comment' => '#649987',
    'field_collection_item' => '#00A0B0',
    'commerce_product' => '#FFCC00',
    'commerce_order' => '#FF0066',
    'commerce_line_item' => '#996600',
  );

  public function __construct() {
    $entities = entity_get_info();
    foreach ($entities as $entity_name => $entity) {
      $color = !empty($this->entity_colors[$entity_name]) ? $this->entity_colors[$entity_name] : EC_DEFAULT_COLOR;
      if (!empty($entity['bundles'])) {
        foreach ($entity['bundles'] as $bundle => $bundle_info) {
          if (in_array($entity_name, array('node', 'taxonomy_term'))) {
            $this->nodes[$entity_name . '__' . $bundle] = array(
              'name' => $bundle_info['label'],
              'color' => $color,
            );
          }
          else {
            $name = $bundle_info['label'];
            if ($entity_name == 'commerce_line_item') {
              $name = 'Commerce line item: '. $name;
            }
            $this->known_entities[$entity_name . '__' . $bundle] = array(
              'name' => $name,
              'color' => $color,
            );
          }
        }
      }
      else {
        $this->known_entities[$entity_name . '__' . $entity_name] = array(
          'name' => $entity['label'],
          'color' => $color,
        );
      }
    }
  }

  public function collectFieldCollections() {
    foreach (field_info_fields() as $field_name => $field_info) {
      if ($field_info['type'] == 'field_collection') {
        if (!empty($field_info['bundles'])) {
          foreach ($field_info['bundles'] as $entity_name => $bundles) {
            foreach ($bundles as $bundles_name) {
              $key = $entity_name . '__' . $bundles_name;
              $this->field_collection_bundles[$field_name][] = $key;

              $this->searchKnownEntities($key);
            }
          }
        }
        //dpm($field_name .'|'. $field_info['bundles']['node'][0]);
      }
    }
    //dpm($this->field_collection_bundles);
  }

  public function findConnections() {
    foreach (field_info_fields() as $field_name => $field_info) {
      if (in_array($field_info['type'], $this->available_field_type)) {
        // term_reference
        if (!empty($field_info['settings']['allowed_values'])) {
          foreach ($field_info['settings']['allowed_values'] as $bundle) {
            $this->addBundleConnections(
              $field_info,
              'taxonomy_term__' . $bundle['vocabulary']
            );
          }
        }
        //node_reference
        elseif (!empty($field_info['settings']['referenceable_types'])) {
          foreach ($field_info['settings']['referenceable_types'] as $bundle) {
            if ($bundle) {
              $this->addBundleConnections(
                $field_info,
                'node__' . $bundle
              );
            }
          }
        }
        // entityreference with bundle
        elseif (!empty($field_info['settings']['handler_settings']['target_bundles'])) {
          foreach ($field_info['settings']['handler_settings']['target_bundles'] as $bundle) {
            $key = $field_info['settings']['target_type'] . '__' . $bundle;
            $this->addBundleConnections(
              $field_info,
              $key
            );
            if (!isset($this->nodes[$key])) {
              $this->nodes[$key] = $key;
            }
          }
        }
        // entityreference user
        elseif (!empty($field_info['settings']['target_type']) && $field_info['settings']['target_type'] == 'user') {
          $this->nodes['user'] = t('User');
          $this->addBundleConnections(
            $field_info,
            'user__user'
          );
        }
        // entityreference file
        elseif (!empty($field_info['settings']['target_type']) && $field_info['settings']['target_type'] == 'file') {
          $this->addBundleConnections(
            $field_info,
            'file__file'
          );
        }
        // entityreference commerce_discounts
        elseif (!empty($field_info['settings']['target_type']) && $field_info['settings']['target_type'] == 'commerce_discount') {
          $this->addBundleConnections(
            $field_info,
            'commerce_discount__order_discount'//@TODO: temporary solution
          );
        }
        // commerce_product_reference
        elseif ($field_info['type'] == 'commerce_product_reference') {
          $this->addBundleConnections(
            $field_info,
            'commerce_product__product'//@TODO: this can be other, not just product
          );
        }
        // commerce_line_item_reference
        elseif ($field_info['type'] == 'commerce_line_item_reference') {
          $this->addBundleConnections(
            $field_info,
            'commerce_line_item__product'//@TODO: this can be other, not just product
          );
        }
        else {
          if (module_exists('devel')) {
            dpm($field_info);
          }
          drupal_set_message(
            t(
              'Can\'t find connections in %fieldname',
              array('%fieldname' => $field_name)
            ),
            'warning',
            FALSE
          );
        }
      }
    }
  }

  /**
   * Creates all connection to bundle
   */
  public function addBundleConnections($field_info, $bundle_to) {
    $show_label = variable_get('entity_connections_display_labels', 0);
    foreach ($field_info['bundles'] as $entity_name => $bundle_names) {
      foreach ($bundle_names as $bundle_name) {
        $key = $entity_name . '__'. $bundle_name;
        if ($entity_name == 'field_collection_item') {
          //dpm($field_info);
          //dpm($key);
          //dpm($bundle_name);
          //dpm($bundle_to);
          $this->nodes[$key] = array(
            'name' => $bundle_name,
            'color' => $this->entity_colors[$entity_name],
          );
          if (isset($this->field_collection_bundles[$bundle_name])) {
            foreach ($this->field_collection_bundles[$bundle_name] as $pre_connection_bundle) {
              $this->connections[] = array(
                'from' => $pre_connection_bundle,
                'to' => $key,
                'color' => '#bebfb9',
                'label' => $show_label ? $bundle_name : '',
              );
            }
            unset($this->field_collection_bundles[$bundle_name]);
          }
        }

        $this->searchKnownEntities($key);
        $this->searchKnownEntities($bundle_to);

        // TODO: ezt kesobb meg kell oldani, ilyen ne lehessen
        if (empty($this->nodes[$key])) {
          $this->nodes[$key] = array(
            'name' => t('Unknown') . ': ' . $key,
            'color' => EC_DEFAULT_COLOR,
          );
        }
        $this->connections[] = array(
          'from' => $key,
          'to' => $bundle_to,
          'color' => ($field_info['cardinality']) == 1 ? '#00A0B0' : 'orange',
          'label' => $show_label ? $field_info['field_name'] : '',
        );
      }
    }
    if (empty($this->nodes[$bundle_to])) {
      $this->nodes[$bundle_to] = array(
        'name' => t('Unknown') . ': ' . $bundle_to,
        'color' => EC_DEFAULT_COLOR,
      );
    }
  }


  public function searchKnownEntities($key) {
    if (empty($this->nodes[$key]) && !empty($this->known_entities[$key])) {
      $this->nodes[$key] = $this->known_entities[$key];
    }
  }

  public function getNodes() {
    return $this->nodes;
  }

  public function getConnections() {
    return $this->connections;
  }

  public function getEdges() {
    $r = array();
    foreach ($this->connections as $con) {
      $r[$con['from']][$con['to']] = array(
        'color' =>  $con['color'],
        'name' =>  $con['label'],
        'directed'  =>  true,
      );
    }
    return $r;
  }

}

class FieldEntityConnections extends EntityConnections {

  /**
   * Override the original funcion
   */
  public function findConnections() {
    $field_connections = array();
    $graph_nodes_from_cnt = array();
    foreach (field_info_fields() as $field_name => $field_info) {
      $this->nodes[$field_name] = array(
        'name' => $field_name,
        'color' => '#ff6666',
      );
      if (!empty($field_info['bundles'])) {
        foreach ($field_info['bundles'] as $entity_name => $bundle_names) {
          foreach ($bundle_names as $bundle_name) {
            $key = $entity_name . '__' . $bundle_name;
            $this->searchKnownEntities($key);
            if ($entity_name == 'field_collection_item') {
              $graph_nodes_from_cnt[$key] = 2; // Fix it to always show this entity
            }
            if (empty($graph_nodes_from_cnt[$field_name])) {
              $graph_nodes_from_cnt[$field_name] = 1;
            }
            else {
              $graph_nodes_from_cnt[$field_name]++;
            }
            $field_connections[] = array(
              'from' => $field_name,
              'to' => $key,
              'color' => EC_DEFAULT_COLOR,
              'label' => '',
            );

          }
        }
      }
    }
    //dpm($graph_nodes_from_cnt);
    foreach ($field_connections as $connection) {
      if ($graph_nodes_from_cnt[$connection['from']] > 1) {
        $this->connections[] = $connection;
      }
      else {
        //dpm($connection);
        unset($this->nodes[$connection['from']]);
      }
    }
  }

}
