TABLE OF CONTENTS
-----------------
 * Overview
 * Requirements
 * Installation Paths

Overview
--------
This module draws the connections between entities. Supported node_reference, term_reference and entity_reference.
The visualization supported by Arbor javascript library.

Requirements
------------
  - Drupal 7.x
  - PHP 5.2 or greater

Installation Paths
------------------
It is recommended to install the Entity connections for Drupal module in the "sites/all/modules" directory.


